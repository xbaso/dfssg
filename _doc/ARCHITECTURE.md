
# Classes

- Field: Field is in principle  definiiton of data saved inside of every tile f Field and then the update function. It aslo has name, the shortcut name etc.
- FieldInstance: Is a instance of field definition in particular Space instance.
- Space: Space is a collection of various fields, it has capability to check if all Fields in given PSace are defined okay etc..
- SpaceInstance: Is an instance of Space., it has defined a size and contains the Instances of FIeld
- 


## Requierements

- We can create various Fields.
    - of different scale.
    - different type (float, vec2, vec3, vec4)
- We can have various Instances of spaces, which each other has a instance of Fields.
- We can use different strategies for running simulation.
    - Data, Logic should be combined in SimulationInstance.
    - Space and Feild Definitions should be together
- Field and Spaces has to have unique identifier.

- some fields should have different size.  

## Notes:

- whn reading from files, we maybe want multiple reading strategies, for example as a math formula or as direct code etc...

# Revisions

##  rev1 - Base
In this revision we want to create a base functionality only around Fields and Spaces.
```cpp

class Manager {
    private std::vector<Field> _fields;
    private std::vector<Space> _spaces;
    private std::vector<SimulationStrategy> _simStrategies;

    public newField(Field field);
    public newSpace(Space space);
    public newSimulationStrategy(SimulationStrategy simulaitonStrategy);
}

class Field {
    private std::string _shortcut; //?? change this
    private std::string _name;

    public Field(std::string shortcut, std::string name);

    public FieldInstance createInstance(SpaceInstance spaceInstance);
    public void registerNewStrategy<T>(FieldUpdateStrategy<T> &fieldUpdateStrategy, T strategy);

    static public Field LoadFromStr(std::string str);
}

class FieldInstance {
    private SpaceInstance _spraceInstance;
    private Field _field;

    public FieldInstance(Field field, SpaceInstance space);

    public int getWidth(); //get trough SpaceInstance
    public int getHeight(); //get trough SpaceInstance

}

class Space {
    private std::vector<Field> _fields;

    public addField(Field field);
    public bool checkIntegrity();

    public createInstance<SimulationStrategy>(int width, int height, bool cyclic); // runs checkIntegrity().

    static public Space LoadFromStr(std::string str);
    static public Space LoadFromFile(std::string path);
}

class SpaceInstance {
    private int _width;
    private int _height;
    private bool _cyclic;

    private std::Array<FieldInstance>: _fieldInstances; 

    public SpaceInstance(Space space);
    public bool checkIntegrity();

    public int getWidth(); //get trough SpaceInstance
    public int getHeight(); //get trough SpaceInstance
    public FieldInstance getField(string fieldId);

}

// this class have a funcitonality to have a single simulation
// SimulationRunType is type, that is used to run the strategy
// in case of OpenGL it is GLSL code, in case of C it is void (*T)(SpaceInstance &si)
class FieldUpdateStrategy<SimulationRunType> {
    private std::string Field;

    runStep()
}

// this class have a funcitonality to solve given Simulation by either
class SimulationStrategy<SimulationRunType> {
    private std::map<std::string, FieldUpdateStrategy<SimulationRunType>> strategies;

    public addFieldUpdateStrategy(FieldUpdateStrategy fieldUpdateStrategy);
}


class SimulationStrategyInstance {
    public bool compile(); // will prepare the simulation
    public bool runStep(); // run one step of simulation
}
```

### Problems:
- no dependent fields, no way to check the integrity of Space
- how do we choose the

## rev2 - Added Dependent Fields. for a better controll of integrity etc..
In this revision we want to add other Dependent Fields.
znd we remove other things.
```cpp


```

## rev3 - Added Dimensionality

## rev4 - Added Types?? For now only floating numbers??