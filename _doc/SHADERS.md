
```c
// Space Instance Variable Definitions
#define DIM0_SIZE      // int
#define DIM1_SIZE       // int
#define CYCLIC      // bool


// Definitions
#define OUT_OF_BOUNDS_VALUE -1f;


// Neighborhood Definitions

#define NEIGHBORHOOD_SIZE 8

const int NEIGHBORHOOD_X[8] = int[8](...);
const int NEIGHBORHOOD_Y[8] = int[8](...);
#define FOR_NEIGH(x, y) for(int x=-1; x) ... {  { 
// .. returns the for loop to iterate neighborhood!
#define ENDFOR_NEIGH } }



// For now we only have floats

struct Tile {
    #for F in FIELDS
        float F;
    #endfor
};

layout(std430, binding = 3) buffer Space
{
    TIle tiles[];
};



// field functions

#for F in FIELDS
    float F(int x, int y) {

        #if CYCLIC
            x %= DIM1_SIZE;
            y %= DIM0_SIZE;
        #else
            if (x < 0 || x >= DIM1_SIZE || y < 0 || y > DIM0_SIZE) {
                returnOUT_OF_BOUNDS_VALUE;
            }

        #endif

        return FIELDS.F[y * DIM1_SIZE + x];
    }
    float F(float x, float y) {
        //... return linear approximation ... is linear the best? :D
    }

    float gradF(int x, int y) {
        //... returns gradient
    }

    //... other functions.
#endfor


```