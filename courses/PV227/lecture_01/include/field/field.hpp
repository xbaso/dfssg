#pragma once

#include <string>

#include "../universe/universe_instance.hpp"

class FieldInstance;

class Field
{
private:
    std::string _shortcut;
    std::string _name;
    std::vector<std::string> _dependentFields;

public:
    Field(std::string shortcut, std::string name) : _shortcut(shortcut), _name(name){};

    FieldInstance createInstance(UniverseInstance spaceInstance);

    inline void addDependentField(std::string field)
    {
        _dependentFields.push_back(field);
    }
    inline std::string getShortcut()
    {
        return _shortcut;
    }
    inline std::string getName()
    {
        return _name;
    }

    // wtf??
    static Field LoadFromStr(std::string str);
};