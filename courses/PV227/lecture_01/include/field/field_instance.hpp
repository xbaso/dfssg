#pragma once

#include "field.hpp"
#include "../universe/universe_instance.hpp"

class FieldInstance
{
private:
    UniverseInstance _spaceInstance;
    Field _field;

public:
    FieldInstance(Field field, UniverseInstance space) : _field(field), _spaceInstance(space){};

    int getWidth();  // get trough UniverseInstance
    int getHeight(); // get trough UniverseInstance
};