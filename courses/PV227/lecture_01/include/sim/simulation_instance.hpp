#pragma once

#include "../universe/universe_instance.hpp"

template<class SimulationStrategyT>
class SimulationInstance
{
    static_assert(std::is_base_of<SimulationStrategy, SimulationStrategyT>::value, "SimulationInstance must be of SimulationStrategy type");

protected:
    SimulationStrategyT _strategy;
    UniverseInstance _universeInstance;


public:
    SimulationInstance(SimulationStrategyT strategy, UniverseInstance universeInstnace) 
        : _strategy(strategy), _universeInstance(universeInstance) {}

    virtual void init() = 0;
    virtual void runStep() = 0;
    virtual void destroy() = 0;

};