#pragma once

#include <string>

#include "simulation_strategy.hpp"
#include "simulation_instance.hpp"
#include "../universe/universe_instance.hpp"

typedef std::string updateStrategyT;

class SimulationInstance_OpenGL : public SimulationInstance<SimulationStrategy<updateStrategyT>> {
private: 
    // TODO
public:
    SimulationInstance_OpenGL(SimulationStrategy<updateStrategyT> strategy, UniverseInstance universeInstance)
        : SimulationInstance(strategy, universeInstance) {
    }

    void init() override {
        // TODO
    }


    void runStep() override {
        // TODO
    }

    void destroy() override {
        // TODO
    }

};