#pragma once
#include "simulation_instance.hpp"
#include "simulation_strategy.hpp"


typedef void (*updateStrategyT)(const SimulationStrategy_C &strategy);

class SimulationStrategy_C : public SimulationInstance<SimulationStrategy<updateStrategyT>>  {
private: 
    // TODO
public:
    SimulationStrategy_C(SimulationStrategy<updateStrategyT> strategy, UniverseInstance universeInstance)
        : SimulationInstance(strategy, universeInstance) {
    }

    void init() override {
        // TODO
    }

    void runStep() override {
        // TODO
    }

    void destroy() override {
        // TODO
    }

};