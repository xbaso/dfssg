#pragma once

#include <map>

// this class have a funcitonality to solve given Simulation by either
template <typename FieldUpdateStrategyT>
class SimulationStrategy
{
protected:
    std::map<std::string, FieldUpdateStrategyT> _strategies;

public:
    addFieldUpdateStrategy(FieldUpdateStrategyT fieldUpdateStrategy);
};