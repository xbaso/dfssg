#pragma once

#include <string>
#include <vector>
#include <map>
#include <stdexcept>

#include "../field/field.hpp"
#include "../sim/simulation_strategy.hpp"

class Universe
{
private:
    std::string _name;
    std::map<std::string, Field> _fields;

public:
    Universe(std::string name) : _name(name) {}

    inline void addField(Field field) {
        if (_fields.find(field.getName()) != _fields.end())
        {
            throw std::invalid_argument("Field with name " + field.getName() + " already exists in Universe " + _name);
        }
        _fields[field.getName()] = field;
    }

    bool checkIntegrity();

    template <typename T>
    void createInstance(SimulationStrategy<T> strategy,int width, int height, bool cyclic);

    inline std::string getName()
    {
        return _name;
    }

    static Universe LoadFromStr(std::string str);
};