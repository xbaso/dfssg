#pragma once

#include <map>

#include "../field/field_instance.hpp"
#include "universe.hpp"

class UniverseInstance
{
private:
    int _width;
    int _height;
    bool _cyclic;

    std::map<std::string, FieldInstance> _fieldInstances;

public:
    UniverseInstance(Universe space);
    bool checkIntegrity(); // TODO

    // Inline methods
    inline int getWidth() { return _width;}
    inline int getHeight() { return _height;}
    inline FieldInstance getField(std::string fieldId) { return _fieldInstances[fieldId];}
};