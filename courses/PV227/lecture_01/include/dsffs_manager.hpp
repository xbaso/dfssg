#pragma once

#include <vector>
#include "./field/field.hpp"
#include "./universe/universe.hpp"
#include "./sim/simulation_strategy.hpp"
class DFSSGManager {
    private:
        std::map<std::string, Field> _fields;
        std::map<std::string, Universe> _universes;
        //std::vector<SimulationStrategy> _simStrategies;

    public:
        inline void newField(Field field) {
            _fields.insert(std::make_pair(field.getName(), field));
        }
        inline void newUniverse(Universe universe) {
            _universes.insert(std::make_pair(universe.getName(), universe));
        }
};